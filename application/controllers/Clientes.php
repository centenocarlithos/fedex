<?php
class Clientes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Cliente');
        //login
        if(!$this->session->userdata("conectado")){
            redirect('sesiones/login');
        }
    }

    public function nuevo()
    {
        $this->load->view('sesiones/header');
        $this->load->view('clientes/nuevo');
        $this->load->view('sesiones/footer');
    }

    public function lista()
    {
        $data['usuario'] = $this->Cliente->obtenerTodo();
        $this->load->view('sesiones/header');
        $this->load->view('clientes/lista', $data);
        $this->load->view('sesiones/footer');
    }

    public function map_cliente()
    {
        $data['usuario'] = $this->Cliente->obtenerTodos();
        $this->load->view('sesiones/header');
        $this->load->view('clientes/map_cliente', $data);
        $this->load->view('sesiones/footer');
    }

    public function guardar()
    {
        $datosNuevoCliente = array(
            "nombre_user" => $this->input->post('nombre_user'),
            "apellido_user" => $this->input->post('apellido_user'),
            "cedula_user" => $this->input->post('cedula_user'),
            "pais_user" => $this->input->post('pais_user'),
            "correo_user" => $this->input->post('correo_user'),
            "cell_user" => $this->input->post('cell_user'),
            "lat_user" => $this->input->post('lat_user'),
            "lng_user" => $this->input->post('lng_user'),
        );
        if ($this->Cliente->insertar($datosNuevoCliente)) {
            $this->session->set_flashdata("confirmacion", "Cliente guardado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al guardar intente otra vez :/ ");
        }
        redirect('clientes/lista');
    }

    public function eliminar($id_user)
    {
        if ($this->Cliente->borrar($id_user)) {
            $this->session->set_flashdata("confirmacion", "Cliente eliminado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al eliminar intente otra vez :/ ");
        }
        redirect('clientes/lista');

    }

    //funcion que renderiza el formulario editar
    public function editar($id_user)
    {
        $data["clienteEditar"] = $this->Cliente->obtenerUsuarioPorID($id_user);
        $this->load->view('sesiones/header');
        $this->load->view('clientes/editar', $data);
        $this->load->view('sesiones/footer');
    }

    //Proceso de actualizacion
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "nombre_user" => $this->input->post('nombre_user'),
            "apellido_user" => $this->input->post('apellido_user'),
            "cedula_user" => $this->input->post('cedula_user'),
            "pais_user" => $this->input->post('pais_user'),
            "correo_user" => $this->input->post('correo_user'),
            "cell_user" => $this->input->post('cell_user'),
            "lat_user" => $this->input->post('lat_user'),
            "lng_user" => $this->input->post('lng_user'),
        );
        $id_user = $this->input->post("id_user");
        if ($this->Cliente->actualizar($id_user, $datosEditados)) {
            $this->session->set_flashdata("confirmacion", "Cliente actualizado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al actualizar intente otra vez :/ ");
        }
        redirect("clientes/lista");

    }

    
}
?>