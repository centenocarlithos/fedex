<?php
class Pedidos extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Cliente');
        $this->load->model('Sucursal');
        $this->load->model('Pedido');
        //login
        if(!$this->session->userdata("conectado")){
            redirect('sesiones/login');
        }
    }

    public function nuevo()
    {
        $data['usuarios'] = $this->Cliente->obtenerTodo();
        $data['sucursales'] = $this->Sucursal->obtenerTodo();
        $this->load->view('sesiones/header');
        $this->load->view('pedidos/nuevo',$data);
        $this->load->view('sesiones/footer');
    }

    public function lista()
    {
        $data['pedido'] = $this->Pedido->obtenerTodo();
        $this->load->view('sesiones/header');
        $this->load->view('pedidos/lista', $data);
        $this->load->view('sesiones/footer');
    }

    public function map_pedido()
    {
        $data['pedido'] = $this->Pedido->obtenerTodos();
        $this->load->view('sesiones/header');
        $this->load->view('pedidos/map_pedido', $data);
        $this->load->view('sesiones/footer');
    }

    public function guardar()
    {
        $datosNuevoPedido = array(
            "usuario_id_user" => $this->input->post('usuario_id_user'),
            "sucursal_id_suc" => $this->input->post('sucursal_id_suc'),
            "nombre_pedido" => $this->input->post('nombre_pedido'),
            "apellido_pedido" => $this->input->post('apellido_pedido'),
            "telefono_pedido" => $this->input->post('telefono_pedido'),
            "ciudad_pedido" => $this->input->post('ciudad_pedido'),
            "correo_pedido" => $this->input->post('correo_pedido'),
            "direccion_pedido" => $this->input->post('direccion_pedido'),
            "destino_pedido" => $this->input->post('destino_pedido'),
            "numero_pedido" => $this->input->post('numero_pedido'),
            "peso_pedido" => $this->input->post('peso_pedido'),
            "desc_pedido" => $this->input->post('desc_pedido'),
            "inicio_pedido" => $this->input->post('inicio_pedido'),
            "fin_pedido" => $this->input->post('fin_pedido'),
            "estado_pedido" => $this->input->post('estado_pedido'),
            "lat_pedido" => $this->input->post('lat_pedido'),
            "lng_pedido" => $this->input->post('lng_pedido'),
        );
        if ($this->Pedido->insertar($datosNuevoPedido)) {
            $this->session->set_flashdata("confirmacion", "Pedido guardado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al eliminar intente otra vez :/ ");
        }
        redirect('pedidos/lista');
    }

    public function eliminar($id_pedido)
    {
        if ($this->Pedido->borrar($id_pedido)) {
            $this->session->set_flashdata("confirmacion", "Pedido eliminado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al eliminar intente otra vez :/ ");
        }
        redirect('pedidos/lista');

    }

    public function editar($id_pedido)
    {
        $data['usuarios'] = $this->Cliente->obtenerTodo();
        $data['sucursales'] = $this->Sucursal->obtenerTodo();
        $data["pedidoEditar"] = $this->Pedido->obtenerPedidoPorID($id_pedido);
        $this->load->view('sesiones/header');
        $this->load->view('pedidos/editar', $data);
        $this->load->view('sesiones/footer');
    }

    //Proceso de actualizacion
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "usuario_id_user" => $this->input->post('usuario_id_user'),
            "sucursal_id_suc" => $this->input->post('sucursal_id_suc'),
            "nombre_pedido" => $this->input->post('nombre_pedido'),
            "apellido_pedido" => $this->input->post('apellido_pedido'),
            "telefono_pedido" => $this->input->post('telefono_pedido'),
            "ciudad_pedido" => $this->input->post('ciudad_pedido'),
            "correo_pedido" => $this->input->post('correo_pedido'),
            "direccion_pedido" => $this->input->post('direccion_pedido'),
            "destino_pedido" => $this->input->post('destino_pedido'),
            "numero_pedido" => $this->input->post('numero_pedido'),
            "peso_pedido" => $this->input->post('peso_pedido'),
            "desc_pedido" => $this->input->post('desc_pedido'),
            "inicio_pedido" => $this->input->post('inicio_pedido'),
            "fin_pedido" => $this->input->post('fin_pedido'),
            "estado_pedido" => $this->input->post('estado_pedido'),
            "lat_pedido" => $this->input->post('lat_pedido'),
            "lng_pedido" => $this->input->post('lng_pedido'),
        );
        $id_pedido = $this->input->post("id_pedido");
        if ($this->Pedido->actualizar($id_pedido, $datosEditados)) {
            $this->session->set_flashdata("confirmacion", "Pedido actualizado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al actualizar intente otra vez :/ ");
        }
        redirect("pedidos/lista");


    }

    public function ruta($id_pedido)
    {
        $data['usuarios'] = $this->Cliente->obtenerTodo();
        $data['sucursales'] = $this->Sucursal->obtenerTodo();
        $data["pedidoEditar"] = $this->Pedido->obtenerPedidoPorID($id_pedido);
        $this->load->view('sesiones/header');
        $this->load->view('pedidos/ruta',$data);
        $this->load->view('sesiones/footer');
    }

}
?>