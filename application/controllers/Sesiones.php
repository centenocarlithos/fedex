<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sesiones extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Sesion');
    }

    public function login()
    {
        $this->load->view('sesiones/login');
    }

    public function presentacion()
    {
        $this->load->view('sesiones/header');
        $this->load->view('sesiones/presentacion');
        $this->load->view('sesiones/footer');
    }
    public function ingresar() {
            $usuario_log = $this->input->post('usuario_log');
            $pass_log = $this->input->post('pass_log');

            $user = $this->Sesion->login($usuario_log, $pass_log);
            if ($user) {
                $this->session->set_userdata("conectado",$user);
                redirect('sesiones/presentacion');
            } else {
                $this->session->set_flashdata('error_message', 'Nombre de usuario o contraseña incorrecta');
                redirect('sesiones/login');
            }
    }

    public function logout() {
        $this->session->set_userdata('desconectado');
        redirect('sesiones/login');
    }
}
