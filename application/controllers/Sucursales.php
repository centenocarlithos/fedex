<?php
  class Sucursales extends CI_Controller  {

    function __construct()
    {
      parent::__construct();
      $this->load->model('Sucursal');
      //login
      if(!$this->session->userdata("conectado")){
        redirect('sesiones/login');
    }
    }

    public function nuevo(){
          $this->load->view('sesiones/header');
          $this->load->view('sucursales/nuevo');
          $this->load->view('sesiones/footer');
    }

    public function lista(){
          $data['sucursal']=$this->Sucursal->obtenerTodo();
          $this->load->view('sesiones/header');
          $this->load->view('sucursales/lista',$data);
          $this->load->view('sesiones/footer');
    }

    public function map_sucursal(){
          $data['sucursal']=$this->Sucursal->obtenerTodos();
          $this->load->view('sesiones/header');
          $this->load->view('sucursales/map_sucursal',$data);
          $this->load->view('sesiones/footer');
    }

    public function guardar(){
        $datosNuevaSucursal=array(
          "nombre_suc"=>$this->input->post('nombre_suc'),
          "continente_suc"=>$this->input->post('continente_suc'),
          "pais_suc"=>$this->input->post('pais_suc'),
          "provincia_suc"=>$this->input->post('provincia_suc'),
          "ciudad_suc"=>$this->input->post('ciudad_suc'),
          "direcciones_suc"=>$this->input->post('direcciones_suc'),
          "lat_suc"=>$this->input->post('lat_suc'),
          "lng_suc"=>$this->input->post('lng_suc'),
          "abierto_suc"=>$this->input->post('abierto_suc'),
          "cerrado_suc"=>$this->input->post('cerrado_suc'),
        );
        if ($this->Sucursal->insertar($datosNuevaSucursal)) {
            $this->session->set_flashdata("confirmacion", "Sucursal guardado exitosamente");
        } else {
            $this->session->set_flashdata("error", "Error al eliminar intente otra vez :/ ");
        }
        redirect('sucursales/lista');
  }

      public function eliminar($id_suc){
        if ($this->Sucursal->borrar($id_suc)){
          $this->session->set_flashdata("confirmacion", "Sucursal eliminado exitosamente");
        } else {
          $this->session->set_flashdata("error", "Error al actualizar intente otra vez :/ ");
        }
        redirect("sucursales/lista");
  }

    public function editar($id_suc)
    {
      $data["sucursalEditar"] = $this->Sucursal->obtenerSucursalPorID($id_suc);
      $this->load->view('sesiones/header');
      $this->load->view('sucursales/editar', $data);
      $this->load->view('sesiones/footer');
    }

    //Proceso de actualizacion
    public function procesarActualizacion()
    {
      $datosEditados = array(
        "nombre_suc" => $this->input->post('nombre_suc'),
        "continente_suc" => $this->input->post('continente_suc'),
        "pais_suc" => $this->input->post('pais_suc'),
        "provincia_suc" => $this->input->post('provincia_suc'),
        "ciudad_suc" => $this->input->post('ciudad_suc'),
        "direcciones_suc" => $this->input->post('direcciones_suc'),
        "lat_suc" => $this->input->post('lat_suc'),
        "lng_suc" => $this->input->post('lng_suc'),
        "abierto_suc" => $this->input->post('abierto_suc'),
        "cerrado_suc" => $this->input->post('cerrado_suc'),
      );
      $id_suc = $this->input->post("id_suc");
      if ($this->Sucursal->actualizar($id_suc, $datosEditados)) {
        $this->session->set_flashdata("confirmacion", "Sucursal actualizado exitosamente");
      } else {
          $this->session->set_flashdata("error", "Error al actualizar intente otra vez :/ ");
      }
      redirect("sucursales/lista");


  }

}
 ?>
