<?php
class Cliente extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos)
    {
        return $this->db->insert("usuario",$datos);
    }

    function obtenerTodo()
    {
        $listadoClientes =
            $this->db->get("usuario");
        //VALIDACION
        if (
            $listadoClientes
                ->num_rows() > 0
        ) {
            return $listadoClientes->result();
        } else {
            return false;
        }
    }


    function borrar($id_user)
    {
        $this->db->where("id_user", $id_user);
        return $this->db->delete("usuario");
    }
    //MAPA GENERAL
    function obtenerTodos()
    {
        $listadoClientes = $this->db->get("usuario");
        if (
            $listadoClientes->num_rows()
            > 0
        ) {
            return $listadoClientes->result();
        }
        return false;
    }

    function obtenerUsuarioPorID($id_user)
    {
        $this->db->where("id_user", $id_user);
        $query = $this->db->get("usuario");
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function actualizar($id_user, $datos)
    {
        $this->db->where("id_user", $id_user);
        return $this->db->update('usuario', $datos);
    }



} //Cierre de la clase

?>