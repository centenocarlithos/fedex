<?php
class Pedido extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    //Funcion para insertar un instructor en MYSQL
    function insertar($datos)
    {
        return $this->db
            ->insert(
                "pedido",
                $datos
            );
    }

    function obtenerTodo()
    {
        $listadoPedidos =
            $this->db->get("pedido");
        //VALIDACION
        if (
            $listadoPedidos
                ->num_rows() > 0
        ) {
            return $listadoPedidos->result();
        } else {
            return false;
        }
    }


    function borrar($id_pedido)
    {
        $this->db->where("id_pedido", $id_pedido);
        return $this->db->delete("pedido");
    }
    //MAPA GENERAL
    function obtenerTodos()
    {
        $listadoPedidos = $this->db->get("pedido");
        if (
            $listadoPedidos->num_rows()
            > 0
        ) {
            return $listadoPedidos->result();
        }
        return false;
    }

    function obtenerPedidoPorID($id_pedido)
    {
        $this->db->where("id_pedido", $id_pedido);
        $query = $this->db->get("pedido");
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function actualizar($id_pedido, $datos)
    {
        $this->db->where("id_pedido", $id_pedido);
        return $this->db->update('pedido', $datos);
    }


} //Cierre de la clase

?>