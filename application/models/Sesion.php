<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sesion extends CI_Model {

public function login($usuario_log, $pass_log) {
    $this->db->where('usuario_log', $usuario_log);
    $this->db->where('pass_log', $pass_log);
    $query = $this->db->get('login');

    if ($query->num_rows() == 1) {
        return $query->row_array();
    } else {
        return false;
    }
}
}
?>