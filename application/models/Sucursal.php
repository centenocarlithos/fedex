<?php
  class Sucursal extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Funcion para insertar un instructor en MYSQL
    function insertar($datos){
        return $this->db
                ->insert("sucursal",
                $datos);
    }

    function obtenerTodo(){
      $listadoSucursales=
      $this->db->get("sucursal");
      //VALIDACION
      if ($listadoSucursales
        ->num_rows()>0){
          return $listadoSucursales->result();
      } else {
        return false;
      }
    }


    function borrar($id_suc){
      $this->db->where("id_suc",$id_suc);
      return $this->db->delete("sucursal");
    }
//MAPA GENERAL
    function obtenerTodos(){
        $listadoSucursales=$this->db->get("sucursal");
        if ($listadoSucursales->num_rows()
            > 0) {
          return $listadoSucursales->result();
        }
        return false;
      }

  function obtenerSucursalPorID($id_suc)
  {
    $this->db->where("id_suc", $id_suc);
    $query = $this->db->get("sucursal");
    if ($query->num_rows() > 0) {
      return $query->row();
    } else {
      return false;
    }
  }

  function actualizar($id_suc, $datos)
  {
    $this->db->where("id_suc", $id_suc);
    return $this->db->update('sucursal', $datos);
  }
  
  }//Cierre de la clase

 ?>
