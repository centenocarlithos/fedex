<style>
    .form-control,
    .form-select {
        border: 1px solid skyblue;
    }

    label {
        color: black;
    }

    #mapaUbicacion {
        height: 400px;
        width: 50%;
        border: 2px solid black;
        margin: 0 auto;
        /* Add this line to center the map horizontally */
    }

    h2 {
        color: #444;
        background-color: transparent;
        border-bottom: 1px solid #D0D0D0;
        font-size: 19px;
        font-weight: normal;
        margin: 0 0 14px 0;
        padding: 14px 15px 10px 15px;
    }

    body {
        background-color: #fff;
        margin: 40px;
        font: 13px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
    }

    #borde-seccion {
        margin-left: 100px;
    }
</style>
<div id="borde-seccion">
    <h2>Sistema Fedex Clientes</h2>
    <div class="container me-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center bg-primary rounded-4">
                    <div class="row">
                        <div class="col-md-2">
                            &nbsp;
                            <img src="<?php echo base_url(); ?>/plantilla/assets/img/cliente.png" alt="">
                        </div>
                        <div class="col-md-8 align-self-center">
                            <h1 class="text-white">EDITAR CLIENTE</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <form class="" action="<?php echo site_url(); ?>/clientes/procesarActualizacion" method="post" id="frm-cliente">
            <div class="container" id="letra">
                <div class="row">
                        <input type="hidden" name="id_user" id="id_user" value="<?php echo $clienteEditar->id_user; ?>">
                    <div class="col-md-4">
                        <label for="">Nombres:</label>
                        <br>
                        <input type="text" placeholder="Ingrese los nombres" class="form-control" name="nombre_user"
                            value="<?php echo $clienteEditar->nombre_user; ?>" id="nombre_user" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">Apellidos:</label>
                        <br>
                        <input type="text" placeholder="Ingrese los apellidos" class="form-control" name="apellido_user"
                            value="<?php echo $clienteEditar->apellido_user; ?>" id="apellido_user">
                    </div>
                    <div class="col-md-4">
                        <label for="">Cédula:</label>
                        <br>
                        <input type="number" placeholder="Ingrese la cédula " class="form-control" name="cedula_user"
                            value="<?php echo $clienteEditar->cedula_user; ?>" id="cedula_user">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="">País:</label>
                        <br>
                        <input type="text" placeholder="Ingrese el país" class="form-control" name="pais_user" value="<?php echo $clienteEditar->pais_user; ?>"
                            id="pais_user">
                    </div>
                    <div class="col-md-4">
                        <label for="">Correo electrónico:</label>
                        <br>
                        <input type="text" placeholder="Ingrese el correo electrónico " class="form-control"
                            name="correo_user" value="<?php echo $clienteEditar->correo_user; ?>" id="correo_user">
                    </div>
                    <div class="col-md-4">
                        <label for="">Teléfono:</label>
                        <br>
                        <input type="number" placeholder="Ingrese el teléfono" class="form-control" name="cell_user"
                            value="<?php echo $clienteEditar->cell_user; ?>" id="cell_user">
                    </div>
                </div>
                <br>
                <div class="row">
                    <h1 class="text-center">COORDENADAS UBICACIÓN CLIENTE</h1>
                    <div class="col-md-6">
                        <label for="">Latitud:</label>
                        <br>
                        <input type="text" placeholder="Ingrese la latitud" class="form-control" readonly
                            name="lat_user" value="<?php echo $clienteEditar->lat_user; ?>" id="lat_user">
                    </div>
                    <div class="col-md-6">
                        <label for="">Longitud:</label>
                        <br>
                        <input type="text" placeholder="Ingrese la longitud" class="form-control" readonly
                            name="lng_user" value="<?php echo $clienteEditar->lng_user; ?>" id="lng_user">
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div id="mapaUbicacion" style="height:400px; width:60%; border:2px solid black"></div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-primary">
                            Editar
                        </button>
                        &nbsp;
                        <a href="<?php echo site_url(); ?>/clientes/nuevo" class="btn btn-danger">
                            Cancelar
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    function initMap() {
        var coordenadaCentry = new google.maps.LatLng(-1.3819240058299596, -78.68875618841503);
        var mapa5 = new google.maps.Map(document.getElementById('mapaUbicacion'), {
            center: coordenadaCentry,
            zoom: 3,
            mapTypeId: 'roadmap'
        });

        var marcador2 = new google.maps.Marker({
            position: coordenadaCentry,
            map: mapa5,
            title: "Seleccione la dirección",
            icon: "<?php echo base_url(); ?>/plantilla/assets/img/cli.png",
            draggable: true
        })
        google.maps.event.addListener(marcador2, 'dragend', function () {
            // alert("Se termino el drags");
            document.getElementById('lat_user').value = this.getPosition().lat();
            document.getElementById('lng_user').value = this.getPosition().lng();
        });
    }
</script>
<script type="text/javascript">
    $("#frm-cliente").validate({
        rules: {
            nombre_user: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true
            },
            apellido_user: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true
            },
            cedula_user: {
                required: true,
                minlength: 10,
                maxlength: 10,
            },
            pais_user: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true
            },
            correo_user: {
                required: true,
                minlength: 3,
                maxlength: 30,
            },
            cell_user: {
                required: true,
                minlength: 10,
                maxlength: 10,
            },
            lat_user: {
                required: true,
            },
            lng_user: {
                required: true,
            },
        },
        messages: {
            nombre_user: {
                required: "Ingrese el nombre del cliente",
                minlength: "Ingrese un nombre válido",
                maxlength: "Nombre erróneo",
            },
            apellido_user: {
                required: "Ingrese el apellido del cliente",
                minlength: "Ingrese un apellido válido",
                maxlength: "Apellido erróneo",
            },
            cedula_user: {
                required: "Ingrese la cédula del cliente",
                minlength: "Ingrese un cédula válido",
                maxlength: "Cédula errónea",
            },
            pais_user: {
                required: "Ingrese el país del cliente",
                minlength: "Ingrese un país válido",
                maxlength: "País erróneo",
            },
            correo_user: {
                required: "Ingrese el correo del cliente",
                minlength: "Ingrese un correo válido",
                maxlength: "Correo erróneo",
            },
            cell_user: {
                required: "Ingrese el celular del cliente",
                minlength: "Ingrese un # de celular válido",
                maxlength: "# de celular erróneo",
            },
            lat_user: {
                required: "Seleccione una ubicación en el mapa",
            },
            lng_user: {
                required: "Seleccione una ubicación en el mapa",
            },
        } //Fin de rules
    })
</script>