<style>
  .table-heading {
    color: black;
  }

  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }
</style>
<h2>Sistema Fedex Clientes</h2>
<div id="borde-seccion">
  <div class="container me-5">
    <div class="row" style="background-color:rgb(5,99,187); border-radius: 5px; ">
      <div class="col-md-9 d-flex justify-content-center align-items-center">
        <h1 style="color:white">LISTA DE CLIENTES</h1>
      </div>
      <div class="col-md-3 d-flex justify-content-between align-items-center">
        <a href="<?php echo site_url(); ?>/Clientes/nuevo" class="btn btn-success"><i class="bi bi-person-plus"></i>&nbsp;&nbsp;Agregar Cliente</a>
      </div>
    </div>
  </div>


  <br>
  <!-- ifelse y tabulador -->
  <?php if ($usuario) : ?>
    <div class="container me-5">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="table-heading">NOMBRES</th>
              <th class="table-heading">APELLIDOS</th>
              <th class="table-heading">CÉDULA</th>
              <th class="table-heading">PAÍS</th>
              <th class="table-heading">CORREO ELECTRÓNICO</th>
              <th class="table-heading">TELÉFONO</th>
              <th class="table-heading">LATITUD</th>
              <th class="table-heading">LONGITUD</th>
              <th class="table-heading">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($usuario as $filatemporal) : ?>
              <tr>
                <td>
                  <?php echo $filatemporal->nombre_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->apellido_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->cedula_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->pais_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->correo_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->cell_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->lat_user ?>
                </td>
                <td>
                  <?php echo $filatemporal->lng_user ?>
                </td>
                <td class="text-center">
                  <a href="<?php echo site_url(); ?>/Clientes/editar/<?php echo $filatemporal->id_user?>" title="Editar Cliente" style="color:green;"><i class="bi bi-pencil-square"></i></a>
                  &nbsp;
                  <a href="<?php echo site_url(); ?>/Clientes/eliminar/<?php echo $filatemporal->id_user ?>" title="Eliminar Cliente" style="color:red;" onclick="return confirm('¿Está seguro de eliminar de forma permanente el registro seleccionado?')"><i class="bi bi-person-x"></i></a>
                  &nbsp;
                  <a href="<?php echo site_url(); ?>/clientes/map_cliente/" title="Visualizar Cliente" style="color:blue;"><i class="bi bi-geo-alt"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php else : ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <img src="<?php echo base_url(); ?>/plantilla/assets/img/nodatos.avif" alt="No existen datos" width="100%" height="500px">
        </div>
      </div>
    </div>
  <?php endif; ?>
</div>