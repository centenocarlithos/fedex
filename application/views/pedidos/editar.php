<style>
  .form-control,
  .form-select {
    border: 1px solid skyblue;
  }

  label {
    color: black;
  }

  #mapaUbicacion {
    height: 400px;
    width: 50%;
    border: 2px solid black;
    margin: 0 auto;
    /* Add this line to center the map horizontally */
  }

  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }
</style>
<h2>Sistema Fedex Pedidos</h2>
<div id="borde-seccion">

  <div class="container me-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center bg-primary rounded-4">
          <div class="row">
            <div class="col-md-2">
              &nbsp;
              <img src="<?php echo base_url(); ?>/plantilla/assets/img/pedido.png" alt="">
            </div>
            <div class="col-md-8 align-self-center">
              <h1 class="text-white">NUEVO PEDIDO</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <form class="" action="<?php echo site_url(); ?>/pedidos/procesarActualizacion" method="post" id="frm-pedido">
      <input type="hidden" name="id_pedido" value="<?php echo $pedidoEditar->id_pedido;?>" id="id_pedido" placeholder="<?php echo $pedidoEditar->id_pedido;?>">
      <div class="container" id="letra">
        <h1>DATOS REMITENTE</h1>
        <div class="row">
          <div class="col-md-6">
            <label for="usuario_id_user">Cédula Remitente:</label>
            <select class="form-select" name="usuario_id_user" id="usuario_id_user" required>
                <option  value="<?php echo $pedidoEditar->usuario_id_user?>"><?php $usuario = $this->Cliente->obtenerUsuarioPorID($pedidoEditar->usuario_id_user);echo $usuario->cedula_user;?></option>
              <optgroup label="Cambie la cédula del pedido">
              <?php foreach ($usuarios as $usuario) : ?>
                <option value="<?php echo $usuario->id_user; ?>"><?php echo $usuario->cedula_user; ?>
                </option>
              <?php endforeach; ?>
              </optgroup>
            </select>
          </div>
          <div class="col-md-6">
            <label for="sucursal_id_suc">Sucursal Remitente:</label>
            <select class="form-select" name="sucursal_id_suc" id="sucursal_id_suc" required>
              
                <option  value="<?php echo $pedidoEditar->sucursal_id_suc?>"><?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);echo $sucursal->nombre_suc;?></option>
              
              <optgroup label="Cambie la sucursal del pedido">
              <?php foreach ($sucursales as $sucursal) : ?>
                <option value="<?php echo $sucursal->id_suc; ?>"><?php echo $sucursal->nombre_suc; ?>
                </option>
              <?php endforeach; ?>
              </optgroup>
            </select>
          </div>
        </div>
        <h1>DATOS DESTINATARIO</h1>
        <div class="row">
          <div class="col-md-4">
            <label for="">Nombres:</label>
            <br>
            <input type="text" placeholder="Ingrese los nombres" class="form-control" name="nombre_pedido" value="<?php echo $pedidoEditar->nombre_pedido?>" id="nombre_pedido">
          </div>
          <div class="col-md-4">
            <label for="">Apellidos:</label>
            <br>
            <input type="text" placeholder="Ingrese los apellidos" class="form-control" name="apellido_pedido" value="<?php echo $pedidoEditar->apellido_pedido?>" id="apellido_pedido">
          </div>
          <div class="col-md-4">
            <label for="">Teléfono:</label>
            <br>
            <input type="number" placeholder="Ingrese el # teléfono" class="form-control" name="telefono_pedido" value="<?php echo $pedidoEditar->telefono_pedido?>" id="telefono_pedido">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label for="">Ciudad:</label>
            <br>
            <input type="text" placeholder="Ingrese la ciudad" class="form-control" name="ciudad_pedido" value="<?php echo $pedidoEditar->ciudad_pedido?>" id="ciudad_pedido">
          </div>
          <div class="col-md-4">
            <label for="">Correo electrónico:</label>
            <br>
            <input type="text" placeholder="Ingrese el correo electrónico" class="form-control" name="correo_pedido" value="<?php echo $pedidoEditar->correo_pedido?>" id="correo_pedido">
          </div>
          <div class="col-md-4">
            <label for="">Dirección:</label>
            <br>
            <input type="text" placeholder="Ingrese la dirección" class="form-control" name="direccion_pedido" value="<?php echo $pedidoEditar->direccion_pedido?>" id="direccion_pedido">
          </div>
        </div>
        <h1>DATOS DEL PAQUETE</h1>
        <div class="row">
          <div class="col-md-4">
            <label for="">País de Destino:</label>
            <br>
            <input type="text" placeholder="Ingrese el pais" class="form-control" name="destino_pedido" value="<?php echo $pedidoEditar->destino_pedido?>" id="destino_pedido">
          </div>
          <div class="col-md-2">
            <label for="">N° de Pedidos:</label>
            <br>
            <input type="number" placeholder="Ingrese el # de pedido" class="form-control" name="numero_pedido" value="<?php echo $pedidoEditar->numero_pedido?>" id="numero_pedido">
          </div>
          <div class="col-md-2">
            <label for="">Peso en libras:</label>
            <br>
            <input type="number" placeholder="Ingrese el peso del paquete" class="form-control" name="peso_pedido" value="<?php echo $pedidoEditar->peso_pedido?>" id="peso_pedido">
          </div>
          <div class="col-md-4">
            <label for="">Descripción:</label>
            <br>
            <input type="text" placeholder="Ingrese una descripción del paquete" class="form-control" name="desc_pedido" value="<?php echo $pedidoEditar->desc_pedido?>" id="desc_pedido">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label for="">Fecha de Envío:</label>
            <br>
            <input type="text" class="form-control" name="inicio_pedido" value="<?php echo $pedidoEditar->inicio_pedido?>" id="inicio_pedido" readonly>
          </div>
          <div class="col-md-4">
            <label for="">Fecha de Entrega (Estimada):</label>
            <br>
            <input type="date" class="form-control" name="fin_pedido" value="<?php echo $pedidoEditar->fin_pedido?>" id="fin_pedido" min="<?php echo $pedidoEditar->inicio_pedido?>">
          </div>
          <div class="col-md-4">
            <label for="">Estado:</label>
            <select class="form-select" name="estado_pedido" id="estado_pedido">
              <optgroup label="Estado del pedido">
                <option value="<?php echo $pedidoEditar->estado_pedido?>"><?php echo $pedidoEditar->estado_pedido?></option>
              </optgroup>
              <option value="En tránsito">En tránsito</option>
              <option value="Pendiente">Pendiente</option>
              <option value="Entregado">Entregado</option>
            </select>
          </div>
        </div>
        <br>
        <div class="row">
          <h1 class="text-center">COORDENADAS UBICACIÓN DE DESTINO PEDIDO</h1>
          <div class="col-md-6">
            <label for="">Latitud:</label>
            <br>
            <input type="text" placeholder="Ingrese la latitud" class="form-control" readonly name="lat_pedido" value="<?php echo $pedidoEditar->lat_pedido?>" id="lat_pedido">
          </div>
          <div class="col-md-6">
            <label for="">Longitud:</label>
            <br>
            <input type="text" placeholder="Ingrese la longitud" class="form-control" readonly name="lng_pedido" value="<?php echo $pedidoEditar->lng_pedido?>" id="lng_pedido">
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12">
            <div id="mapaUbicacion" style="height:400px; width:60%; border:2px solid black"></div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
              Actualizar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/pedidos/nuevo" class="btn btn-danger">
              Cancelar
            </a>
          </div>
        </div>
      </div>
    </form>
  </div>

</div>

<script type="text/javascript">
  function initMap() {
    var coordenadaCentry = new google.maps.LatLng(-1.3819240058299596, -78.68875618841503);
    var mapa5 = new google.maps.Map(document.getElementById('mapaUbicacion'), {
      center: coordenadaCentry,
      zoom: 7,
      mapTypeId: 'roadmap'
    });

    var marcador2 = new google.maps.Marker({
      position: coordenadaCentry,
      map: mapa5,
      title: "Seleccione la dirección",
      icon: "<?php echo base_url(); ?>/plantilla/assets/img/puntero.png",
      draggable: true
    })
    google.maps.event.addListener(marcador2, 'dragend', function() {
      // alert("Se termino el drags");
      document.getElementById('lat_pedido').value = this.getPosition().lat();
      document.getElementById('lng_pedido').value = this.getPosition().lng();
    });
  }
</script>
<script type="text/javascript">
  $("#frm-pedido").validate({
    rules: {
      //DATOS DEL DESTINATARIO
      nombre_pedido: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      apellido_pedido: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      telefono_pedido: {
        required: true,
        minlength: 3,
        maxlength: 10,
      },
      ciudad_pedido: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      correo_pedido: {
        required: true,
        minlength: 3,
        maxlength: 30,
      },
      direccion_pedido: {
        required: true,
        minlength: 3,
        maxlength: 500,
      },
      //DATOS DEL PAQUETE
      destino_pedido: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      numero_pedido: {
        required: true,
        minlength: 1,
        maxlength: 30,

      },
      peso_pedido: {
        required: true,
        minlength: 0.1,
        maxlength: 30,
      },
      desc_pedido: {
        required: true,
        minlength: 3,
      },
      inicio_pedido: {
        required: true,
      },
      fin_pedido: {
        required: true,
      },
      estado_pedido: {
        required: true,
      },
      lat_pedido: {
        required: true,
      },
      lng_pedido: {
        required: true,
      },
    }, //Fin de rules
    messages: {
      //DATOS DEL DESTINATARIO
      nombre_pedido: {
        required: "Ingrese el nombre del destinatario",
        minlength: "Ingrese un nombre válido",
        maxlength: "Nombre erróneo",
      },
      apellido_pedido: {
        required: "Ingrese el apellido del destinatario",
        minlength: "Ingrese un apellido válido",
        maxlength: "Apellido erróneo",
      },
      telefono_pedido: {
        required: "Ingrese el celular del destinatario",
        minlength: "Ingrese un # de celular válido",
        maxlength: "# de celular erróneo",
      },
      ciudad_pedido: {
        required: "Ingrese la ciudad del destinatario",
        minlength: "Ingrese una ciudad válida",
        maxlength: "Ciudad erróneo",
      },
      correo_pedido: {
        required: "Ingrese el correo del destinatario",
        minlength: "Ingrese un correo válido",
        maxlength: "Correo erróneo",
      },
      direccion_pedido: {
        required: "Ingrese la dirección del destinatario",
        minlength: "Ingrese un dirección válida",
        maxlength: "Dirección errónea",
      },
      //DATOS DEL PAQUETE
      destino_pedido: {
        required: "Ingrese el país de destino",
        minlength: "Ingrese un país válido",
        maxlength: "País erróneo",
      },
      numero_pedido: {
        required: "Ingrese el # del pedido",
        minlength: "Ingrese un # válido",
        maxlength: "Error dato fuera de rango",
      },
      peso_pedido: {
        required: "Ingrese el peso del paquete",
        minlength: "Ingrese un valor válido",
        maxlength: "Error valor fuera de rango",
      },
      desc_pedido: {
        required: "Ingrese una descripción del pedido",
        minlength: "Ingrese una descripción válida",
      },
      inicio_pedido: {
        required: "Ingrese una fecha de inicio",
      },
      fin_pedido: {
        required: "Ingrese una fecha de fin",
      },
      estado_pedido: {
        required: "Ingrese el estado del pedido",
      },
      lat_pedido: {
        required: "Seleccione una ubicación en el mapa",
      },
      lng_pedido: {
        required: "Seleccione una ubicación en el mapa",
      },
    }
  })
</script>