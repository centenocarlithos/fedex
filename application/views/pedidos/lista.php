<style>
  .table-heading {
    color: black;
  }

  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }
</style>
<h2>Sistema Fedex Pedidos</h2>
<div id="borde-seccion">
  <div class="container me-5">
    <div class="row" style="background-color:rgb(5,99,187); border-radius: 5px;">
      <div class="col-md-9 d-flex justify-content-center align-items-center">
        <h1 style="color:white">LISTA DE PEDIDOS</h1>
      </div>
      <div class="col-md-3 d-flex justify-content-between align-items-center">
        <a href="<?php echo site_url(); ?>/Pedidos/nuevo" class="btn btn-success"><i class="bi bi-person-plus"></i>Agregar Pedido</a>
      </div>
    </div>
  </div>


  <br>
  <!-- ifelse y tabulador -->
  <?php if ($pedido) : ?>
    <div class="container me-5">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="table-heading">REMITENTE</th>
              <th class="table-heading">CÉDULA</th>
              <th class="table-heading">SUCURSAL REMITENTE</th>
              <th class="table-heading">DESTINATARIO</th>
              <th class="table-heading">TELÉFONO</th>
              <th class="table-heading">CIUDAD</th>
              <th class="table-heading">CORREO ELECTRÓNICO</th>
              <th class="table-heading">DIRECCIÓN</th>
              <th class="table-heading">DESTINO</th>
              <th class="table-heading">NÚMERO</th>
              <th class="table-heading">PESO (LB)</th>
              <th class="table-heading">DESCRIPCIÓN</th>
              <th class="table-heading">FECHA INICIO</th>
              <th class="table-heading">FECHA FIN</th>
              <th class="table-heading">ESTADO PEDIDO</th>
              <th class="table-heading">LATITUD</th>
              <th class="table-heading">LONGITUD</th>
              <th class="table-heading">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($pedido as $filatemporal) : ?>
              <tr>
                <td>
                  <?php
                  $usuario = $this->Cliente->obtenerUsuarioPorID($filatemporal->usuario_id_user);
                  if ($usuario) {
                    echo $usuario->nombre_user . ' ' . $usuario->apellido_user;
                  } else {
                    echo "Usuario no encontrado";
                  }
                  ?>
                </td>
                <td>
                  <?php
                  $usuario = $this->Cliente->obtenerUsuarioPorID($filatemporal->usuario_id_user);
                  if ($usuario) {
                    echo $usuario->cedula_user;
                  } else {
                    echo "Usuario no encontrado";
                  }
                  ?>
                </td>
                <td>
                  <?php
                  $sucursal = $this->Sucursal->obtenerSucursalPorID($filatemporal->sucursal_id_suc);
                  if ($sucursal) {
                    echo $sucursal->nombre_suc;
                  } else {
                    echo "Sucursal no encontrada";
                  }
                  ?>
                </td>
                <td>
                  <?php echo $filatemporal->nombre_pedido . ' ' . $filatemporal->apellido_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->telefono_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->ciudad_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->correo_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->direccion_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->destino_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->numero_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->peso_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->desc_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->inicio_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->fin_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->estado_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->lat_pedido ?>
                </td>
                <td>
                  <?php echo $filatemporal->lng_pedido ?>
                </td>
                <td class="text-center">
                  <a href="<?php echo site_url(); ?>/Pedidos/editar/<?php echo $filatemporal->id_pedido ?>" title="Editar Pedido" style="color:green;"><i class="bi bi-pencil-square"></i></a>
                  &nbsp;
                  <a href="<?php echo site_url(); ?>/Pedidos/eliminar/<?php echo $filatemporal->id_pedido ?>" title="Eliminar Pedido" style="color:red;" onclick="return confirm('¿Está seguro de eliminar de forma permanente el registro seleccionado?')"><i class="bi bi-person-x"></i></a>
                  &nbsp;
                  <a href="<?php echo site_url(); ?>/pedidos/ruta/<?php echo $filatemporal->id_pedido ?>" title="Visualizar Pedido" style="color:blue;"><i class="bi bi-geo-alt"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php else : ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <img src="<?php echo base_url(); ?>/plantilla/assets/img/nodatos.avif" alt="No existen datos" width="100%" height="500px">
        </div>
      </div>
    </div>
  <?php endif; ?>
</div>