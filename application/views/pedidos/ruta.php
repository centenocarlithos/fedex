<style>
  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }

  #sidebar {
    flex-basis: 15rem;
    flex-grow: 1;
    padding: 1rem;
    max-width: 30rem;
    height: 100%;
    box-sizing: border-box;
    overflow: auto;
  }
</style>
<h2>Sistema Fedex Pedidos</h2>
<div id="borde-seccion">
  <div class="container text-center me-5" style="background-color:rgb(5,99,187); border-radius: 5px;">
    &nbsp;
    <h1 style="color:white;">Ruta Pedidos</h1>
    &nbsp;
  </div>
  <br>
  <div class="row">
    <div class="col-md-4">
      <label for="">Id del pedido:</label>
      <br>
      <input type="text" name="id_pedido" class="form-control" value="<?php echo $pedidoEditar->id_pedido; ?>" id="id_pedido" placeholder="<?php echo $pedidoEditar->id_pedido; ?>" readonly>
    </div>
    <div class="col-md-4">
      <label for="">Origen:</label>
      <br>
      <input type="text" placeholder="Ingrese los apellidos" class="form-control" name="" value="<?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
                                                                                                  echo $sucursal->nombre_suc; ?>" id="" readonly>
    </div>
    <div class="col-md-4">
      <label for="">Destino:</label>
      <br>
      <input type="text" placeholder="Ingrese el # teléfono" class="form-control" name="" value="<?php echo $pedidoEditar->ciudad_pedido ?>" id="" readonly>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4">
      <label for="">Fecha de Envío:</label>
      <br>
      <input type="text" class="form-control" name="inicio_pedido" value="<?php echo $pedidoEditar->inicio_pedido ?>" id="inicio_pedido" readonly>
    </div>
    <div class="col-md-4">
      <label for="">Fecha de Entrega (Estimada):</label>
      <br>
      <input type="text" class="form-control" name="fin_pedido" value="<?php echo $pedidoEditar->fin_pedido ?>" id="fin_pedido" min="<?php echo $pedidoEditar->inicio_pedido ?>">
    </div>
    <div class="col-md-4">
      <label for="">Estado:</label>
      <br>
      <input type="text" class="form-control" name="inicio_pedido" value="<?php echo $pedidoEditar->estado_pedido ?>" id="inicio_pedido" readonly>
    </div>
  </div>
  <hr>
  <div class="row">
    <h3 class="text-center">Coordenadas</h3>
    <div class="col-md-6">
      <div class="row">
        <h4 class="text-center">Origen</h4>
        <div class="col-md-6">
          <h5 for="" class="text-center">Latitud</h5>
          <p class="text-center form-control"><?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
                                              echo $sucursal->lat_suc; ?></p>
        </div>
        <div class="col-md-6">
          <h5 for="" class="text-center">Longitud</h5>
          <p class="text-center form-control"><?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
                                              echo $sucursal->lng_suc; ?></p>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <h4 class="text-center">Destino</h4>
        <div class="col-md-6">
          <h5 for="" class="text-center">Latitud</h5>
          <label class="text-center form-control"><?php echo $pedidoEditar->lat_pedido ?></label>
        </div>
        <div class="col-md-6">
          <h5 for="" class="text-center">Longitud</h5>
          <p class="text-center form-control"><?php echo $pedidoEditar->lng_pedido ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<div class="container me-5">
  <div class="col-md-12">
    <div id="mapaNacional" style="height:600px; width:100%; border:2px solid black;"></div>
  </div>
</div>
</div>

<script type="text/javascript">
  function initMap() {
    var centro = new google.maps.LatLng(-1.8317532479234215, -78.18306382144804);
    var origen = {
      lat: <?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
            echo $sucursal->lat_suc; ?>,
      lng: <?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
            echo $sucursal->lng_suc; ?>
    };
    var destino = {
      lat: <?php echo $pedidoEditar->lat_pedido; ?>,
      lng: <?php echo $pedidoEditar->lng_pedido; ?>
    };

    var directionsService = new google.maps.DirectionsService();
    var directionsRenderer = new google.maps.DirectionsRenderer();

    mapa = new google.maps.Map(document.getElementById("mapaNacional"), {
      center: centro,
      zoom: 7,
    });

    directionsRenderer.setMap(mapa);

    calcularRuta(directionsService, directionsRenderer, origen, destino);
  }

  function calcularRuta(directionsService, directionsRenderer, origen, destino) {
    directionsService.route({
        origin: origen,
        destination: destino,
        travelMode: google.maps.TravelMode.DRIVING,
      },
      function(response, status) {
        if (status === "OK") {
          directionsRenderer.setDirections(response);
        } else {
          //Alerta
          
          // Define los puntos de la polilínea
          var flightPlanCoordinates = [{
              lat: <?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
            echo $sucursal->lat_suc; ?>,
              lng: <?php $sucursal = $this->Sucursal->obtenerSucursalPorID($pedidoEditar->sucursal_id_suc);
            echo $sucursal->lng_suc; ?>
            }, // Punto 1
            {
              lat: <?php echo $pedidoEditar->lat_pedido; ?>,
              lng: <?php echo $pedidoEditar->lng_pedido; ?>
            } // Punto 2
          ];
          // Crea la polilínea
          var flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates, // Puntos de la polilínea
            geodesic: true, // Utiliza rutas geodésicas (curvas) en lugar de líneas rectas
            strokeColor: '#FF0000', // Color de la línea (en este caso, rojo)
            strokeOpacity: 1.0, // Opacidad de la línea (de 0.0 a 1.0)
            strokeWeight: 2 // Grosor de la línea
          });
          flightPath.setMap(mapa);
          window.alert("No se pudo calcular la ruta de conduccion: " + status + " Calculando el origen y destino de forma poligonal");
        }
      }
    );
  }
</script>