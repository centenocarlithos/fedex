<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
    toastr.success("<?php echo $this->session->flashdata("confirmacion"); ?>");
  </script>
  <?php $this->session->set_flashdata("confirmacion", "") ?>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
    toastr.error("<?php echo $this->session->flashdata("error"); ?>");
  </script>
  <?php $this->session->set_flashdata("error", "") ?>
<?php endif; ?>

<!-- ======= Footer ======= -->
<footer id="footer">
  <div class="container me-5">
    <h3>Sistema Fedex</h3>
    <p>FedEx te ofrece soluciones y un servicio en los cuales puedes confiar.</p>
    <div class="social-links">
      <a href="https://twitter.com/fedex" target="_blank" class="twitter"><i class="bx bxl-twitter"></i></a>
      <a href="https://www.facebook.com/FedEx/" target="_blank" class="facebook"><i class="bx bxl-facebook"></i></a>
      <a href="https://www.youtube.com/user/fedexamericalatina" target="_blank" class="youtube"><i class="bx bxl-youtube"></i></a>
      <a href="https://www.linkedin.com/showcase/fedex-am%25C3%25A9rica-latina/?_l=es_ES" target="_blank" class="linkedin"><i class="bx bxl-linkedin"></i></a>
    </div>
    <div class="copyright">
      &copy; Copyright <strong><span>Carlos Centeno and Santiago Loachamin</span></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: [license-url] -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-html-bootstrap-template-my-resume/ -->
    </div>
  </div>
</footer><!-- End Footer -->
<div id="preloader"></div>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/purecounter/purecounter_vanilla.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/aos/aos.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/swiper/swiper-bundle.min.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/typed.js/typed.umd.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/waypoints/noframework.waypoints.js"></script>
<script src="<?php echo base_url(); ?>/plantilla/assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="<?php echo base_url(); ?>/plantilla/assets/js/main.js"></script>

</body>

</html>