<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SISTEMA FEDEX</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>/plantilla/assets/img/logo.png" rel="icon">
  <link href="<?php echo base_url(); ?>/plantilla/assets/img/logo.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">


  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url(); ?>/plantilla/assets/css/style.css" rel="stylesheet">

  <!-- Google API -->
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXSZ-J8_TIIP3yCdoCBD1Al60r9s8M_iM&libraries=places&callback=initMap"></script>
  <script async defer src="https://routes.googleapis.com/directions/v2:computeRoutes?key=AIzaSyCXSZ-J8_TIIP3yCdoCBD1Al60r9s8M_iM"></script>
  <!-- Google API -->
  <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/javascript">
    jQuery.validator.addMethod("letras", function(value, element) {
      return this.optional(element) || /^[A-Za-zñÑáÁéÉíÍóÓúÚüÜ ]*$/.test(value);
    }, "Este campo solo acepta letras");
  </script>
  <!-- IMPORTACION DE TOASTR.JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />


</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <!-- <button type="button" class="mobile-nav-toggle d-xl-none"><i class="bi bi-list mobile-nav-toggle"></i></button> -->
  <i class="bi bi-list mobile-nav-toggle d-lg-none"></i>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex flex-column justify-content-center">

    <nav id="navbar" class="navbar nav-menu">
      <ul>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="true"><i class="bx bx-store-alt"></i> <span>Sucursal</span></a>
          <ul class="dropdown-menu ">
            <a href="<?php echo site_url() ?>/sucursales/nuevo"><i class="bi bi-file-earmark-plus"></i><span>Registro</span></a>
            <a href="<?php echo site_url() ?>/sucursales/lista"><i class="bi bi-card-list"></i> <span>Lista</span></a>
            <a href="<?php echo site_url() ?>/sucursales/map_sucursal"><i class="bi bi-geo-alt"></i><span>Mapa</span></a>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="true">
            <i class="bx bx-user-circle"></i> <span>Cliente</span>
          </a>
          <ul class="dropdown-menu">
            <a href="<?php echo site_url() ?>/clientes/nuevo"><i class="bx bx-user-plus"></i><span>Registro</span></a>
            <a href="<?php echo site_url() ?>/clientes/lista"><i class="bi bi-person-lines-fill"></i> <span>Lista</span></a>
            <a href="<?php echo site_url() ?>/clientes/map_cliente"><i class="bi bi-geo-alt"></i><span>Mapa</span></a>
          </ul>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="true">
            <i class="bi bi-box-seam"></i> <span>Pedido</span>
          </a>
          <ul class="dropdown-menu">
            <a href="<?php echo site_url() ?>/pedidos/nuevo"><i class="bi bi-bag-plus"></i><span>Registro</span></a>
            <a href="<?php echo site_url() ?>/pedidos/lista"><i class="bi bi-card-checklist"></i> <span>Lista</span></a>
            <a href="<?php echo site_url() ?>/pedidos/map_pedido"><i class="bi bi-geo-alt"></i><span>Mapa</span></a>
          </ul>
        </li>
        <li><a href="<?php echo site_url() ?>/sesiones/logout"><i class="bi bi-box-arrow-right  "></i><span>Salir</span></a></li>
      </ul>
    </nav>
    <!-- .nav-menu -->
  </header><!-- End Header -->