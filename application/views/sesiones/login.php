<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SISTEMA FEDEX</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>/plantilla/assets/img/logo.png" rel="icon">
  <link href="<?php echo base_url(); ?>/plantilla/assets/img/logo.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">


  <!-- Vendor CSS Files -->
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>/plantilla/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo base_url(); ?>/plantilla/assets/css/style.css" rel="stylesheet">


  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoObNZz2rn6AMxGvMKq1GDTFvd7CzGwdY&libraries=places&callback=initMap">
  </script>

  <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/javascript">
    jQuery.validator.addMethod("letras", function(value, element) {
      return this.optional(element) || /^[A-Za-zñÑáÁéÉíÍóÓúÚüÜ ]*$/.test(value);
    }, "Este campo solo acepta letras");
  </script>

</head>

<body style="background-color: #f7f8f9">

  <h2 style="color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    " class="text-center">Sistema Fedex Iniciar Sesión</h2>

  <div class="d-flex justify-content-center align-items-center" style="background-color: #f7f8f9; height: 94vh;">
    <div class="container-sm text-center text-light" style="width: 40%; background-color: rgb(0,122,255); border-radius: 20px;">
      <h1 class="fs-2 fw-bold">Login</h1>
      <form action="<?php echo site_url('sesiones/ingresar'); ?>" method="post" id="frm-login">
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label fs-5">Ingrese su usuario:</label>
          <input type="text" class="form-control" id="usuario_log" name="usuario_log" value="">
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label fs-5">Ingrese su contraseña:</label>
          <input type="password" class="form-control" id="pass_log" name="pass_log" value="">
        </div>
        <button type="submit" class="btn btn-light text-primary">Ingresar</button>
        <br><br>
        <?php if ($this->session->flashdata('error_message')) { ?>
        <p style="color:white; background-color: rgb(255,255,255,0.5);"><?php echo $this->session->flashdata('error_message'); ?></p>
    <?php } ?>
      </form>
    </div>
  </div>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/typed.js/typed.umd.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="<?php echo base_url(); ?>/plantilla/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url(); ?>/plantilla/assets/js/main.js"></script>

</body>
<script type="text/javascript">
  $("#frm-login").validate({
    rules: {
      //DATOS DEL DESTINATARIO
      usuario_log:{
        required: true,
        minlength: 3,
        maxlength: 30,
      },
      pass_log:{
        required: true,
        minlength: 3,
        maxlength: 30,
      }
      
    }, //Fin de rules
    messages: {
      usuario_log:{
        required: "Ingrese su usuario",
        minlength: "Ingrese un usuario válido",
        maxlength: "Usuario incorrecto",
      },
      pass_log:{
        required: "Ingrese su contraseña",
        minlength: "Ingrese una contraseña válida",
        maxlength: "Contraseña incorrecta",
      }
    }
  })
</script>
</html>