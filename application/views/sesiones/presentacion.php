<style>/*--------------------------------------------------------------
  # banner-inicio Section
--------------------------------------------------------------*/
		#banner-inicio {
			width: 100%;
			height: 100vh;
			background: url("<?php echo base_url('plantilla/assets/img/s.png') ?>") top right no-repeat;
			background-size: cover;
			position: relative;
		}

		@media (min-width: 992px) {
			#banner-inicio {
				padding-left: 160px;
			}
		}

		#banner-inicio:before {
			content: "";
			background: rgba(255, 255, 255, 0.8);
			position: absolute;
			bottom: 0;
			top: 0;
			left: 0;
			right: 0;
		}

		#banner-inicio h1 {
			margin: 0;
			font-size: 64px;
			font-weight: 700;
			line-height: 56px;
			color: #45505b;
		}

		#banner-inicio p {
			color: #45505b;
			margin: 15px 0 0 0;
			font-size: 26px;
			font-family: "Poppins", sans-serif;
		}

		#banner-inicio p span {
			color: #0563bb;
			letter-spacing: 1px;
		}

		#banner-inicio .social-links {
			margin-top: 30px;
		}

		#banner-inicio .social-links a {
			font-size: 24px;
			display: inline-block;
			color: #45505b;
			line-height: 1;
			margin-right: 20px;
			transition: 0.3s;
		}

		#banner-inicio .social-links a:hover {
			color: #0563bb;
		}

		@media (max-width: 992px) {
			#banner-inicio {
				text-align: center;
			}

			#banner-inicio h1 {
				font-size: 32px;
				line-height: 36px;
			}

			#banner-inicio p {
				margin-top: 10px;
				font-size: 20px;
				line-height: 24px;
			}
		}
</style>

<section id="banner-inicio" class="d-flex flex-column justify-content-center">
		<div class="container" data-aos="zoom-in" data-aos-delay="100">
			<h1>Bienvenido</h1>
			<p>FedEx te ofrece soluciones y un servicio en los cuales puedes confiar.</p>
			<p>Acciones de a realizar <span class="typed" data-typed-items="Clientes, Sucursales, Pedidos"></span></p>
			<p>Realizamos envíos de paquetes livianos o pesados, envíos con carácter de urgente o sin tanta prisa</p>
			<div class="social-links">
			</div>
		</div>
	</section>

