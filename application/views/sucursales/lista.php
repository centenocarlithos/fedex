<style>
  .table-heading {
    color: black;
  }

  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }
</style>
<h2>Sistema Fedex Sucursal</h2>
<div id="borde-seccion">
  <div class="container me-5 text-center">
    <div class="row" style="background-color:rgb(5,99,187); border-radius: 5px;">
      <div class="col-md-9 d-flex justify-content-center align-items-center">
        <h1 style="color:white">LISTA DE SUCURSALES</h1>
      </div>
      <div class="col-md-3 d-flex justify-content-between align-items-center">
        <a href="<?php echo site_url(); ?>/Sucursales/nuevo" class="btn btn-success"><i class="bi bi-person-plus"></i>&nbsp;&nbsp; Agregar Sucursal</a>
      </div>
    </div>
  </div>


  <br>
  <!-- ifelse y tabulador -->
  <?php if ($sucursal) : ?>
    <div class="container me-5">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th class="table-heading">NOMBRES</th>
              <th class="table-heading">CONTINENTE</th>
              <th class="table-heading">PAIS</th>
              <th class="table-heading">PROVINCIA</th>
              <th class="table-heading">CIUDAD</th>
              <th class="table-heading">DIRECCIÓN</th>
              <th class="table-heading">APERTURA</th>
              <th class="table-heading">CIERRE</th>
              <th class="table-heading">LATITUD</th>
              <th class="table-heading">LONGITUD</th>
              <th class="table-heading">ACCIONES</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($sucursal as $filatemporal) : ?>
              <tr>
                <td>
                  <?php echo $filatemporal->nombre_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->continente_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->pais_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->provincia_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->ciudad_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->direcciones_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->abierto_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->cerrado_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->lat_suc ?>
                </td>
                <td>
                  <?php echo $filatemporal->lng_suc ?>
                </td>
                <td class="text-center">
                  <a href="<?php echo site_url(); ?>/Sucursales/editar/<?php echo $filatemporal->id_suc ?>" title="Editar Sucursal" style="color:green;"><i class="bi bi-pencil-square"></i></a>
                  &nbsp;
                  <a href="<?php echo site_url(); ?>/Sucursales/eliminar/<?php echo $filatemporal->id_suc ?>" title="Eliminar Sucursal" style="color:red;" onclick="return confirm('¿Está seguro de eliminar de forma permanente el registro seleccionado?')"><i class="bi bi-trash"></i></a>
                  &nbsp;
                  <a href="<?php echo site_url(); ?>/Sucursales/map_sucursal/" title="Visualizar Sucursal" style="color:blue;"><i class="bi bi-geo-alt"></i></a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php else : ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <img src="<?php echo base_url(); ?>/plantilla/assets/img/nodatos.avif" alt="No existen datos" width="100%" height="500px">
        </div>
      </div>
    </div>
  <?php endif; ?>
</div>