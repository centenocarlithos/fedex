<style>
  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }
</style>
<h2>Sistema Fedex Sucursal</h2>
<div id="borde-seccion">
  <div class="container text-center me-5" style="background-color:rgb(5,99,187); border-radius: 5px;">
    &nbsp;
    <h1 style="color:white;">REPORTE SUCURSALES</h1>
    &nbsp;
  </div>
  <br>
  <div class="container me-5">
    <div class="col-md-12">
      <div id="mapaNacional" style="height:600px; width:100%; border:5px solid rgb(5,99,187); border-radius: 5px;"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function initMap() {
    var centro = new google.maps.LatLng(-0.17834732047773233, -78.46352701164128);
    var mapaNacional = new google.maps.Map(
      document.getElementById('mapaNacional'), {
        center: centro,
        zoom: 3,
        mapTypeId: google.maps.MapTypeId.SATELITAL
      }
    );
    <?php if ($sucursal) : ?>
      <?php foreach ($sucursal as $nacTemporal) : ?>
        var coordenadaNac = new google.maps.LatLng(<?php echo $nacTemporal->lat_suc; ?>, <?php echo $nacTemporal->lng_suc; ?>);
        var marcadorpre = new google.maps.Marker({
          position: coordenadaNac,
          title: "<?php echo $nacTemporal->nombre_suc; ?>",
          icon: "<?php echo base_url(); ?>/plantilla/assets/img/sucursal.png",
          map: mapaNacional
        });
      <?php endforeach; ?>
    <?php endif; ?>
  }
</script>