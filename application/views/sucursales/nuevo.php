<style>
  .form-control,
  .form-select {
    border: 1px solid skyblue;
  }

  label {
    color: black;
  }

  #mapaUbicacion {
    height: 400px;
    width: 50%;
    border: 2px solid black;
    margin: 0 auto;
    /* Add this line to center the map horizontally */

  }

  h2 {
    color: #444;
    background-color: transparent;
    border-bottom: 1px solid #D0D0D0;
    font-size: 19px;
    font-weight: normal;
    margin: 0 0 14px 0;
    padding: 14px 15px 10px 15px;
  }

  body {
    background-color: #fff;
    margin: 40px;
    font: 13px/20px normal Helvetica, Arial, sans-serif;
    color: #4F5155;
  }

  #borde-seccion {
    margin-left: 100px;
  }
</style>
<h2>Sistema Fedex Sucursal</h2>
<div id="borde-seccion">
  <div class="container me-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center bg-primary rounded-4">
          <div class="row">
            <div class="col-md-2">
              &nbsp;
              <img src="<?php echo base_url(); ?>/plantilla/assets/img/tienda.png" alt="">
            </div>
            <div class="col-md-8 align-self-center">
              <h1 class="text-white">NUEVA SUCURSAL</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <form class="" action="<?php echo site_url(); ?>/sucursales/guardar" method="post" id="frm-sucural">
      <div class="container" id="letra">
        <div class="row">
          <div class="col-md-4">
            <label for="">Nombre Sucursal:</label>
            <br>
            <input type="text" placeholder="Ingrese la sucursal" class="form-control" name="nombre_suc" value="" id="nombre_suc">
          </div>
          <div class="col-md-4">
            <label for="">Continente:</label>
            <select class="form-select" name="continente_suc" id="continente_suc">
              <option selected>Seleccione el continente de la sucursal</option>
              <option value="Asia">Asia</option>
              <option value="África">África</option>
              <option value="América del Norte">América del Norte</option>
              <option value="América del Sur">América del Sur</option>
              <option value="Antártida">Antártida</option>
              <option value="Europa">Europa</option>
              <option value="Oceanía">Oceanía</option>
            </select>
          </div>
          <div class="col-md-4">
            <label for="">País:</label>
            <br>
            <input type="text" placeholder="Ingrese el país " class="form-control" name="pais_suc" value="" id="pais_suc">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <label for="">Provincia:</label>
            <br>
            <input type="text" placeholder="Ingrese la provincia" class="form-control" name="provincia_suc" value="" id="provincia_suc">
          </div>
          <div class="col-md-4">
            <label for="">Ciudad:</label>
            <br>
            <input type="text" placeholder="Ingrese la ciudad " class="form-control" name="ciudad_suc" value="" id="ciudad_suc">
          </div>
          <div class="col-md-4">
            <label for="">Dirección:</label>
            <br>
            <input type="text" placeholder="Ingrese la dirección" class="form-control" name="direcciones_suc" value="" id="direcciones_suc">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <label for="">Horario Apertura:</label>
            <br>
            <input type="time" class="form-control" name="abierto_suc" value="" id="abierto_suc">
          </div>
          <div class="col-md-6">
            <label for="">Horario Cierre:</label>
            <br>
            <input type="time" class="form-control" name="cerrado_suc" value="" id="cerrado_suc">
          </div>
        </div>
        <br>
        <div class="row">
          <h1 class="text-center">COORDENADAS UBICACIÓN SUCURSAL</h1>
          <div class="col-md-6">
            <label for="">Latitud:</label>
            <br>
            <input type="text" placeholder="Ingrese la latitud" class="form-control" readonly name="lat_suc" value="" id="lat_suc">
          </div>
          <div class="col-md-6">
            <label for="">Longitud:</label>
            <br>
            <input type="text" placeholder="Ingrese la longitud" class="form-control" readonly name="lng_suc" value="" id="lng_suc">
          </div>
        </div>

        <br>
        <div class="row">
          <div class="col-md-12">
            <div id="mapaUbicacion" style="height:400px; width:60%; border:2px solid black"></div>
          </div>
        </div>

        <br>

        <div class="row">
          <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
              Registrar
            </button>
            &nbsp;
            <a href="<?php echo site_url(); ?>/sucursales/nuevo" class="btn btn-danger">
              Cancelar
            </a>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  function initMap() {
    var coordenadaCentry = new google.maps.LatLng(-1.3819240058299596, -78.68875618841503);
    var mapa5 = new google.maps.Map(document.getElementById('mapaUbicacion'), {
      center: coordenadaCentry,
      zoom: 3,
      mapTypeId: 'roadmap'
    });

    var marcador2 = new google.maps.Marker({
      position: coordenadaCentry,
      map: mapa5,
      title: "Seleccione la dirección",
      icon: "<?php echo base_url(); ?>/plantilla/assets/img/sucurpoint.png",
      draggable: true
    })
    google.maps.event.addListener(marcador2, 'dragend', function() {
      // alert("Se termino el drags");
      document.getElementById('lat_suc').value = this.getPosition().lat();
      document.getElementById('lng_suc').value = this.getPosition().lng();
    });
  }
</script>
<script type="text/javascript">
  $("#frm-sucural").validate({
    rules: {
      nombre_suc: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      pais_suc: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      provincia_suc: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      ciudad_suc: {
        required: true,
        minlength: 3,
        maxlength: 30,
        letras: true
      },
      direcciones_suc: {
        required: true,
        minlength: 3,
        maxlength: 500,
      },
      abierto_suc: {
        required: true,
      },
      cerrado_suc: {
        required: true,
      },
      lat_suc: {
        required: true,
      },
      lng_suc: {
        required: true,
      },
    },
    messages: {
      nombre_suc: {
        required: "Ingrese el nombre de la sucursal",
        minlength: "Ingrese un nombre válido",
        maxlength: "Nombre fuera de rango",
        letras: true
      },
      pais_suc: {
        required: "Ingrese el país de la sucursal",
        minlength: "Ingrese un país válido",
        maxlength: "País erróneo",
      },
      provincia_suc: {
        required: "Ingrese la provincia de la sucursal",
        minlength: "Ingrese un provincia válido",
        maxlength: "Provincia erróneo",
      },
      ciudad_suc: {
        required: "Ingrese la ciudad de la sucursal",
        minlength: "Ingrese una ciudad válido",
        maxlength: "Ciudad erróneo",
      },
      direcciones_suc: {
        required: "Ingrese una dirección de la sucursal",
        minlength: "Ingrese un dirección válida",
        maxlength: "Dirección errónea",
      },
      abierto_suc: {
        required: "Ingrese una hora de apertura",
      },
      cerrado_suc: {
        required: "Ingrese una hora de cierre",
      },
      lat_suc: {
        required: "Seleccione una ubicación en el mapa",
      },
      lng_suc: {
        required: "Seleccione una ubicación en el mapa",
      },
    } //Fin de rules
  })
</script>