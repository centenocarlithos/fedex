<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}

		/*--------------------------------------------------------------
# banner-inicio Section
--------------------------------------------------------------*/
		#banner-inicio {
			width: 100%;
			height: 100vh;
			background: url("<?php echo base_url('plantilla/assets/img/s.png') ?>") top right no-repeat;
			background-size: cover;
			position: relative;
		}

		@media (min-width: 992px) {
			#banner-inicio {
				padding-left: 160px;
			}
		}

		#banner-inicio:before {
			content: "";
			background: rgba(255, 255, 255, 0.8);
			position: absolute;
			bottom: 0;
			top: 0;
			left: 0;
			right: 0;
		}

		#banner-inicio h1 {
			margin: 0;
			font-size: 64px;
			font-weight: 700;
			line-height: 56px;
			color: #45505b;
		}

		#banner-inicio p {
			color: #45505b;
			margin: 15px 0 0 0;
			font-size: 26px;
			font-family: "Poppins", sans-serif;
		}

		#banner-inicio p span {
			color: #0563bb;
			letter-spacing: 1px;
		}

		#banner-inicio .social-links {
			margin-top: 30px;
		}

		#banner-inicio .social-links a {
			font-size: 24px;
			display: inline-block;
			color: #45505b;
			line-height: 1;
			margin-right: 20px;
			transition: 0.3s;
		}

		#banner-inicio .social-links a:hover {
			color: #0563bb;
		}

		@media (max-width: 992px) {
			#banner-inicio {
				text-align: center;
			}

			#banner-inicio h1 {
				font-size: 32px;
				line-height: 36px;
			}

			#banner-inicio p {
				margin-top: 10px;
				font-size: 20px;
				line-height: 24px;
			}
		}

		/*--------------------------------------------------------------
# banner-inicio-2 Section
--------------------------------------------------------------*/
		#banner-inicio-2 {
			width: 100%;
			height: 100vh;
			background: url("<?php echo base_url('plantilla/assets/img/a.jpg') ?>") top right no-repeat;
			background-size: cover;
			position: relative;
		}

		@media (min-width: 992px) {
			#banner-inicio-2 {
				padding-left: 160px;
			}
		}

		#banner-inicio-2:before {
			content: "";
			background: rgba(255, 255, 255, 0.8);
			position: absolute;
			bottom: 0;
			top: 0;
			left: 0;
			right: 0;
		}

		#banner-inicio-2 h1 {
			margin: 0;
			font-size: 64px;
			font-weight: 700;
			line-height: 56px;
			color: #45505b;
		}

		#banner-inicio-2 p {
			color: #45505b;
			margin: 15px 0 0 0;
			font-size: 26px;
			font-family: "Poppins", sans-serif;
		}

		#banner-inicio-2 p span {
			color: #0563bb;
			letter-spacing: 1px;
		}

		#banner-inicio-2 .social-links {
			margin-top: 30px;
		}

		#banner-inicio-2 .social-links a {
			font-size: 24px;
			display: inline-block;
			color: #45505b;
			line-height: 1;
			margin-right: 20px;
			transition: 0.3s;
		}

		#banner-inicio-2 .social-links a:hover {
			color: #0563bb;
		}

		@media (max-width: 992px) {
			#banner-inicio-2 {
				text-align: center;
			}

			#banner-inicio-2 h1 {
				font-size: 32px;
				line-height: 36px;
			}

			#banner-inicio-2 p {
				margin-top: 10px;
				font-size: 20px;
				line-height: 24px;
			}
		}
	</style>
</head>

<body>

	<H1>INICIO</H1>
	<!-- ======= banner-inicio Section ======= -->
	<section id="banner-inicio" class="d-flex flex-column justify-content-center">
		<div class="container" data-aos="zoom-in" data-aos-delay="100">
			<h1>Sistema Fedex</h1>
			<p>FedEx te ofrece soluciones y un servicio en los cuales puedes confiar.</p>
			<p>Para tus necesidades de <span class="typed" data-typed-items="importación, exportación, locales"></span></p>
			<p>Realizamos envíos de paquetes livianos o pesados, envíos con carácter de urgente o sin tanta prisa</p>
			<div class="social-links">
				<a href="https://twitter.com/fedex" target="_blank" class="twitter"><i class="bx bxl-twitter"></i></a>
				<a href="https://www.facebook.com/FedEx/" target="_blank" class="facebook"><i class="bx bxl-facebook"></i></a>
				<a href="https://www.youtube.com/user/fedexamericalatina" target="_blank" class="youtube"><i class="bx bxl-youtube"></i></a>
				<a href="https://www.linkedin.com/showcase/fedex-am%25C3%25A9rica-latina/?_l=es_ES" target="_blank" class="linkedin"><i class="bx bxl-linkedin"></i></a>
			</div>
		</div>
	</section><!-- End banner-inicio -->
	<section id="banner-inicio-2" class="d-flex flex-column justify-content-center">
		<div class="container" data-aos="zoom-in" data-aos-delay="100">
			<div class="row">
				<div class="col-sm-6 mb-3 mb-sm-0">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Misión:</h5>
							<iframe width="500" height="300" src="https://www.youtube.com/embed/ECXFh8vlboQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
							<p class="card-text">Facilitar y agilizar el proceso de envío de paquetes y mercancías, brindando un servicio confiable, eficiente y seguro para satisfacer las necesidades de nuestros clientes en todo el mundo.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Visión:</h5>
							<iframe width="500" height="300" src="https://www.youtube.com/embed/If5ePcada2s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
							<p class="card-text">Ser la plataforma líder a nivel global en servicios de envío, estableciendo una red de colaboración con socios estratégicos y utilizando tecnología de vanguardia para ofrecer soluciones innovadoras.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section><!-- End banner-inicio -->