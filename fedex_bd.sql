-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-06-2023 a las 20:22:05
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `fedex_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `id_pedido` int(11) NOT NULL,
  `usuario_id_user` int(11) NOT NULL,
  `sucursal_id_suc` int(11) NOT NULL,
  `nombre_pedido` varchar(250) DEFAULT NULL,
  `apellido_pedido` varchar(250) DEFAULT NULL,
  `telefono_pedido` varchar(100) DEFAULT NULL,
  `ciudad_pedido` varchar(100) DEFAULT NULL,
  `correo_pedido` varchar(100) DEFAULT NULL,
  `direccion_pedido` varchar(300) DEFAULT NULL,
  `destino_pedido` varchar(255) DEFAULT NULL,
  `numero_pedido` int(11) DEFAULT NULL,
  `peso_pedido` varchar(100) DEFAULT NULL,
  `desc_pedido` varchar(1000) DEFAULT NULL,
  `inicio_pedido` date DEFAULT NULL,
  `fin_pedido` date DEFAULT NULL,
  `estado_pedido` varchar(20) DEFAULT NULL,
  `lat_pedido` float DEFAULT NULL,
  `lng_pedido` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id_pedido`, `usuario_id_user`, `sucursal_id_suc`, `nombre_pedido`, `apellido_pedido`, `telefono_pedido`, `ciudad_pedido`, `correo_pedido`, `direccion_pedido`, `destino_pedido`, `numero_pedido`, `peso_pedido`, `desc_pedido`, `inicio_pedido`, `fin_pedido`, `estado_pedido`, `lat_pedido`, `lng_pedido`) VALUES
(8, 3, 3, 'CARLOS', 'CENTENO', '0987598239', 'Quito', 'centenocarlos182@gmail.com', 'Diego Barba - OE10-7 - El Tránsito', 'Ecuador', 2, '23 lb', 'ZAPATOS', '2002-01-23', '2002-01-25', 'Entregado', -0.580046, -77.2825),
(12, 7, 3, 'MIRIAN', 'ANTE', '09090', 'QUITO', 'mirian@gmail.com', 'Quito Chillogallo', 'ECUADOR', 2, '20 lb', 'cajas', '2024-01-23', '2024-02-23', 'Entregado', -0.360327, -76.5574);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id_suc` int(11) NOT NULL,
  `nombre_suc` varchar(255) DEFAULT NULL,
  `continente_suc` varchar(255) DEFAULT NULL,
  `pais_suc` varchar(255) DEFAULT NULL,
  `provincia_suc` varchar(255) DEFAULT NULL,
  `ciudad_suc` varchar(255) DEFAULT NULL,
  `direcciones_suc` varchar(255) DEFAULT NULL,
  `lat_suc` float DEFAULT NULL,
  `lng_suc` float DEFAULT NULL,
  `abierto_suc` varchar(15) DEFAULT NULL,
  `cerrado_suc` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id_suc`, `nombre_suc`, `continente_suc`, `pais_suc`, `provincia_suc`, `ciudad_suc`, `direcciones_suc`, `lat_suc`, `lng_suc`, `abierto_suc`, `cerrado_suc`) VALUES
(3, 'FEDEX ECUADOR', 'AMERICA', 'ECUADOR', 'PICHINCHA', 'QUITO', '7', 0, 0, '07:07', '07:07'),
(8, 'Brasil FedEx', 'América del Sur', 'Brasil', 'Brasilia', 'Luziana', 'Brasil Luziana', -13.7292, -53.0247, '09:30', '19:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `nombre_user` varchar(255) NOT NULL,
  `apellido_user` varchar(255) NOT NULL,
  `cedula_user` varchar(255) DEFAULT NULL,
  `pais_user` varchar(255) NOT NULL,
  `correo_user` varchar(255) NOT NULL,
  `cell_user` varchar(255) NOT NULL,
  `lat_user` float NOT NULL,
  `lng_user` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_user`, `nombre_user`, `apellido_user`, `cedula_user`, `pais_user`, `correo_user`, `cell_user`, `lat_user`, `lng_user`) VALUES
(3, 'JOSSE', 'PEREZ', '313133', 'Colombia', 'jossegmail.com', '0989786', 0.320821, -78.513),
(7, 'JUAN JOSE', 'FLORES LOPEZ', '1764534213', 'ECUADOR', 'juan@gmail.com', '098978654523', -1.31602, -77.0957),
(8, 'Steve', 'Lopez', '1717171', 'Argentina', 'steve.lopez@gmail.com', '098989', -38.3399, -66.2412);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`id_pedido`),
  ADD KEY `pedido_sucursal_fk` (`sucursal_id_suc`),
  ADD KEY `pedido_usuario_fk` (`usuario_id_user`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id_suc`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `id_pedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id_suc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `pedido_sucursal_fk` FOREIGN KEY (`sucursal_id_suc`) REFERENCES `sucursal` (`id_suc`),
  ADD CONSTRAINT `pedido_usuario_fk` FOREIGN KEY (`usuario_id_user`) REFERENCES `usuario` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
